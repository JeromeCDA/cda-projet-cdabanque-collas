create table agence (
code_agence numeric(3) primary key not null,
nom_agence varchar,
adresse_agence varchar
)

create sequence sq_agence
minvalue 100
start with 100
increment by 1;

create table client (
id_client varchar(8) primary key not null,
nom_client varchar,
prenom varchar,
date_naissance date,
e_mail varchar,
client_actif boolean
)


create table compte (
num_compte numeric(11) primary key not null,
solde float,
decouvert boolean,
id_client varchar(8),
code_agence numeric(3)
)


create sequence sq_compte
minvalue 10000000000
start with 10000000000
increment by 1;


alter table compte add constraint fk_client foreign key (id_client) references client (id_client);
alter table compte add constraint fk_agence foreign key (code_agence) references agence (code_agence);