# README #

Merci d'avoir installé l'application gestion banque

Suivez attentivement les manipulations d'installation afin d'utiliser l'application dans les meilleures conditions :

- Installez pgAdmin 4
- Installez DBEAVER
- Dans la recherche windows, tapez pgAdmin 4, cliquez sur le lien pour accéder au service du software. Un mot de passe de session vous est 
demandé, créez votre mot-de-passe et veillez à le retenir.
- Sur le panneau de contrôle de gauche, déroulez Servers et PostgreSQL si vous ne voyez pas l'onglet Databases
- Clic droit sur l'onglet Databases -> Create -> Database
- Dans le champs Database, tapez en minuscule seulement : cdabanque
- Cliquez sur le bouton Save en bas à droite de la petite fenêtre
- Clic droit sur l'onglet Login/Group Roles -> Create -> Login Group Role
- Dans le champs Name, tapez en minuscules seulement : banquier
- Dans l'onglet Definition de la petite fenêtre Create - Login/Group Role, saisissez dans le champs Password : 123
- Onglet Privileges de la même fenêtre, Activez : "Can login" et "Super User". L'activation de Super User va activer d'autres éléments,
laisser tel quel
- Cliquez sur SAVE
- !!! Laissez l'onglet pgAdmin du navigateur ouvert tant que vous utilisez l'application, il s'agit du serveur nécessaire au transit des données
- Une fois l'installation de DBEAVER terminée, accédez au programme et cliquez sur Nouvelle connexion -> PostgreSQL -> Suivant
- La fenêtre "créer une nouvelle connexion" est ouverte. 
- Dans le champs Database, tapez : cdabanque
- Dans le champs Nom d'utilisateur, tapez : banquier
- Dans le champs mot de passe, tapez : 123
- Cliquer sur Test de la connexion. Si ok, cliquer sur Terminer. Sinon, recommencez.
- Sur le panneau de contrôle de gauche dans DBEAVER, localisez la base de données nouvellement créée et 
faites un clic droit dessus -> Editeur SQL.
- Cliquez sur l'onglet qui vient de s'ouvrir
- Dans le dossier de l'application, ouvrir le fichier "script sql cda banque", copier l'ensemble en faisant
CTRL+A -> CTRL+C -> coller dans l'onglet de DBEAVER relatif à la base de données cdabanque.
- Tout sélectionner et faire ALT+X pour exécuter le script
- Vos tables sont générées !
- Retourner dans le dossier de l'application, ouvrez le fichier runnable.bat