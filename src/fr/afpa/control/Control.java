package fr.afpa.control;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Control {
	
	/**
	 * Pattern de vérification de l'ID
	 * @param idClient
	 * @return
	 */
	public static boolean controlIdClient(String idClient) {
		boolean verif = false;
		String pattern = "[A-Z]{2}\\d{6}";
		if (idClient.matches(pattern)) {
			verif = true;
		}	
		return verif;
	}
	

	/**
	 * Pattern de vérification de la date
	 * @param dateNaissance
	 * @return
	 */
	public static boolean controlFormatDate(String dateNaissance) {
		boolean verif = false;
		String pattern = "((19\\d{2}|20\\d{2})-(0[1-9]|1[0-2])-(0[1-9]|1\\d|2\\d|3[0-1]))";
		if (dateNaissance.matches(pattern)) {
			verif=true;
		}
		return verif;
	}
	
	
	/**
	 * Cette méthode permet de vérifier si un client a atteint la limite de comptes bancaires grâce à la fonction d'aggrégation COUNT. Si la méthode retourne faux, l'utilisateur sort de
	 * la méthode "créer un compte bancaire" afin de pas le bloquer dans une boucle.
	 * @param idClient
	 * @return
	 */
	public static boolean maxAccountsControl(String idClient) {
		
		Connection conn = null;
		PreparedStatement stServices = null;
		ResultSet listeServices = null;
		int accountsPerClient = 0;
		boolean verif = true;
		
		try {
			
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);
			
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/cdabanque", "banquier", "123");
			String strQuery = "select count(id_client) from compte where id_client = ?";
			stServices = conn.prepareStatement(strQuery);
			stServices.setString(1, idClient);
			stServices.executeQuery();

			while (listeServices.next()) {
				accountsPerClient = Integer.parseInt(listeServices.getString("count"));
			}
			
			if (accountsPerClient > 2) {
				verif=false;
				System.out.println("Ce client a déjà 3 comptes et ne peut en avoir plus"+"\n");
			} 

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			
			// Fermeture du RS
			if (listeServices != null)
				try {
					listeServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			// Fermeture du statement
			
			if (stServices != null)
				try {
					stServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			// Fermeture de la connection
			if (conn != null)
				try {

					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			

		}
		return verif;
	}
	
/**
 * Vérification de l'existence du client pour la création de compte	
 * @param idClient
 * @return
 */
public static boolean existClientControl(String idClient) {
		
		Connection conn = null;
		PreparedStatement stServices = null;
		ResultSet listeServices = null;
		boolean verif = true;
		String id = null;
		
		try {
			
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);
			
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/cdabanque", "banquier", "123");
			String strQuery = "select id_client from client where id_client = ?";
			stServices = conn.prepareStatement(strQuery);
			stServices.setString(1, idClient);
			stServices.executeQuery();

			while (listeServices.next()) {
				id = listeServices.getString("id_client");
			}
			
			if (!idClient.equals(id)) {
				verif=false;
				System.out.println("Ce client n'existe pas"+"\n");
			} 

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			
			// Fermeture du RS
			if (listeServices != null)
				try {
					listeServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			// Fermeture du statement
			
			if (stServices != null)
				try {
					stServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			// Fermeture de la connection
			if (conn != null)
				try {

					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			

		}
		return verif;
	}

/**
 * Vérification de l'existence de l'agence en paramètre via codeAgence à destination de la création d'un compte bancaire	
 * @param codeAgence
 * @return
 */
public static boolean existAgenceControl(String codeAgence) {
	
	Connection conn = null;
	PreparedStatement stServices = null;
	ResultSet listeServices = null;
	boolean verif = true;
	String code = null;
	
	try {
		
		String driverName = "org.postgresql.Driver";
		Class.forName(driverName);
		
		conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/cdabanque", "banquier", "123");
		String strQuery = "select code_agence from agence where code_agence = ?";
		stServices = conn.prepareStatement(strQuery);
		stServices.setString(1, codeAgence);
		stServices.executeQuery();

		while (listeServices.next()) {
			code = listeServices.getString("code_agence");
		}
		
		if (!codeAgence.equals(code)) {
			verif=false;
			System.out.println("Cette agence n'existe pas"+"\n");
		} 

	} catch (ClassNotFoundException e) {
		e.printStackTrace();
		
	} catch (SQLException e) {
		e.printStackTrace();
	} finally {
		
		// Fermeture du RS
		if (listeServices != null)
			try {
				listeServices.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		// Fermeture du statement
		
		if (stServices != null)
			try {
				stServices.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		// Fermeture de la connection
		if (conn != null)
			try {

				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		

	}
	return verif;
}

/**
 * Vérification de l'existence d'un compte bancaire
 * @param numCompte
 * @return
 */
public static boolean existAccountControl(String numCompte) {
	
	Connection conn = null;
	PreparedStatement stServices = null;
	ResultSet listeServices = null;
	boolean verif = true;
	String code = null;
	
	try {
		
		String driverName = "org.postgresql.Driver";
		Class.forName(driverName);
		
		conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/cdabanque", "banquier", "123");
		String strQuery = "select num_compte from compte where num_compte = ?";
		stServices = conn.prepareStatement(strQuery);
		stServices.setString(1, numCompte);
		stServices.executeQuery();

		while (listeServices.next()) {
			code = listeServices.getString("num_compte");
		}
		
		if (!numCompte.equals(code)) {
			verif=false;
			System.out.println("Ce compte n'existe pas"+"\n");
		} 

	} catch (ClassNotFoundException e) {
		e.printStackTrace();
		
	} catch (SQLException e) {
		e.printStackTrace();
	} finally {
		
		// Fermeture du RS
		if (listeServices != null)
			try {
				listeServices.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		// Fermeture du statement
		
		if (stServices != null)
			try {
				stServices.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		// Fermeture de la connection
		if (conn != null)
			try {

				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		

	}
	return verif;
}

/**
 * Règle métier : le client doit être actif pour que l'utilisateur puissse accéder à ses informations
 * @param idClient
 * @return
 */
public static boolean isClientActive(String idClient) {
	
	Connection conn = null;
	PreparedStatement stServices = null;
	ResultSet listeServices = null;
	boolean verif = true;
	String code = null;
	
	try {
		
		String driverName = "org.postgresql.Driver";
		Class.forName(driverName);
		
		conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/cdabanque", "banquier", "123");
		String strQuery = "select (case when client_actif=true then 'actif' else 'inactif' end) as client_actif from client where id_client = ?";
		stServices = conn.prepareStatement(strQuery);
		stServices.setString(1, idClient);
		stServices.executeQuery();

		while (listeServices.next()) {
			code = listeServices.getString("client_actif");
		}
		
		if (!"actif".equals(code)) {
			verif=false;
			System.out.println("Ce client est désactivé. Choisissez un client actif"+"\n");
		} 

	} catch (ClassNotFoundException e) {
		e.printStackTrace();
		
	} catch (SQLException e) {
		e.printStackTrace();
	} finally {
		
		// Fermeture du RS
		if (listeServices != null)
			try {
				listeServices.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		// Fermeture du statement
		
		if (stServices != null)
			try {
				stServices.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		// Fermeture de la connection
		if (conn != null)
			try {

				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		

	}
	return verif;
}
	
	
	

}
