package fr.afpa.model;

import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import fr.afpa.control.Control;

public class PrintPDF {
	
	static int count;
	public static final String DEST = "./comptesBancaires.pdf";
	
	
	/**
	 * La méthode de génération de PDF est isolée dans une autre classe modèle de sorte à tendre vers une structure logique
	 */
	public static void genererPDF(String idCt, String nomClient, String prenom, String dateNaissance, String compte, String solde) {
		
		String soldeConvert="";
		try {
			++count;
			Document document = new Document();
			PdfWriter.getInstance(document, new FileOutputStream(DEST));
			document.open();
			
			//insertion id client, nom, prénom et date de naissance
			Paragraph para2 = new Paragraph();
			para2.setAlignment(Element.ALIGN_LEFT);
						
			String line3 = "ID client :        " + idCt+"\n";
			String line4 = "Nom  :            " + nomClient+"\n";
			String line5 = "Prénom :         " + prenom+"\n";
			String line6 = "Naissance :    " + dateNaissance+"\n";
			para2.add(line3);
			para2.add(line4);
			para2.add(line5);
			para2.add(line6);
			
			
			//insertion lignes et colonnes, comptes et soldes + smiley
			
			String line7 = "____________________________________________________________________________";
			String line8 = "Liste de comptes";
			String line9 = "____________________________________________________________________________";
			String line10 = "Numéro de compte           " + "       Solde    " + "              Smiley     ";
			String line11= "____________________________________________________________________________";
			String line12="";
			
			for (int i = 0 ; i<compte.split(";").length ; i++) {
				
				float f = Float.valueOf(solde.split(";")[i]);
				
				if (Math.signum(f)==1.0) {
					soldeConvert = ":-)";
				} else if (Math.signum(f)==-1.0) {
					soldeConvert = ":-(";
				}
				
				line12+=compte.split(";")[i]+"                        "+solde.split(";")[i]+"                  "+soldeConvert+"\n";
			}
			
			Paragraph para = new Paragraph();
			para.setAlignment(Element.ALIGN_LEFT);
			para.add(line7);
			para.add(line8);
			para.add(line9);
			para.add(line10);
			para.add(line11);
			para.add(line12);

			
			document.add(para2);
			document.add(para);
			document.close();
			
			System.out.println("PDF créé"+"\n");
		
			
		}catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("PDF failure");
			e.printStackTrace();
		}
		
			}
	
	
	
	
	
		
	/**
	 * Cette méthode permet de bien dissocier la fonction de génération de pdf et celle de récupération des données de la base de données
	 */
	public static void retrieveAccounts() {
		Scanner in = new Scanner (System.in);
		Connection conn = null;
		PreparedStatement stServices = null;
		ResultSet listeServices = null;
		String idClient=null;
		String compte="";
		String solde="";
		String idCt="";
		String nomClient="";
		String prenom="";
		String dateNaissance="";
		
		try {
			
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);
			
			System.out.println("Tapez l'idClient");
			idClient=in.nextLine();
			if (!Control.isClientActive(idClient)) {
				return;
			}
			
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/cdabanque", "banquier", "123");
			String strQuery = "select c.id_client, c.nom_client, c.prenom, c.date_naissance, cpt.num_compte, round(cast(cpt.solde as numeric), 2) as solde from compte cpt, client c where cpt.id_client = c.id_client and c.id_client = '"+idClient+"'";
			stServices = conn.prepareStatement(strQuery);
			stServices.setString(1, idClient);
			stServices.executeQuery();
			
			
			while (listeServices.next()) {
				
				idCt=listeServices.getString("id_client");
				nomClient=listeServices.getString("nom_client");
				prenom=listeServices.getString("prenom");
				dateNaissance=listeServices.getString("date_naissance");
				compte+=listeServices.getString("num_compte")+";";
				solde+=listeServices.getString("solde")+";";

			}
			
			
			
			genererPDF(idCt, nomClient, prenom, dateNaissance, compte, solde);
	
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
		
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
		
				// Fermeture du RS
				if (listeServices != null)
					try {
						listeServices.close();
					} catch (SQLException e) {
				// TODO Auto-generated catch block
						e.printStackTrace();
			}
		
		// Fermeture du statement
		
				if (stServices != null)
					try {
						stServices.close();
					} catch (SQLException e) {
				// TODO Auto-generated catch block
						e.printStackTrace();
			}
		
		// Fermeture de la connection
		if (conn != null)
			try {

				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		

	}
	}
	
}
	


