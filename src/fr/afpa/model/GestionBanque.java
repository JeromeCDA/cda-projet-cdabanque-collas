package fr.afpa.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Scanner;

import fr.afpa.control.Control;

public class GestionBanque {
	
	
	/**
	 * Utilisation de la séquence pour générer un id unique automatiquement, sans risque de conflits dans la base de données
	 */
	public static void creerAgence() {
		Scanner in = new Scanner(System.in);
		Connection conn = null;
		PreparedStatement stServices = null;
		ResultSet listeServices = null;
		
		try {
			
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);
			
			
			System.out.println("Saisissez le nom de l'agence");
			String nomAgence = in.nextLine();
			System.out.println("Saisissez l'adresse de l'agence");
			String adresseAgence = in.nextLine();
			
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/cdabanque", "banquier", "123");
			
	
			String strQuery = "insert into agence (code_agence, nom_agence, adresse_agence) values (nextval('sq_agence'), ?, ?)";
			stServices = conn.prepareStatement(strQuery);
			stServices.setString(1, nomAgence);
			stServices.setString(2, adresseAgence);
			
			int row = stServices.executeUpdate();

			System.out.println("Agence créée");

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			
			// Fermeture du statement
			
			if (stServices != null)
				try {
					stServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			// Fermeture de la connection
			if (conn != null)
				try {

					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			

		}
	}
	
	/**
	 *  2 regex utilisés dans cette méthode, une pour l'id et l'autre pour la date. J'oblige l'user à la saisir au format US même si la DB convertit automatiquement.
	 *  Catch de la SQLException personnalisée pour informer le client qu'il faut renseigner un ID différent à chaque nouveau client
	 */
	public static void creerClient() {
		Scanner in = new Scanner(System.in);
		Connection conn = null;
		PreparedStatement stServices = null;
		ResultSet listeServices = null;
		String idClient = null;
		String dateNaissance = null;
		String etatClient=null;
		boolean actifInactif=false;
		

		
		try {
			
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);
			
			do {
			System.out.println("Saisissez l'id client en commençant par 2 lettres majuscules puis 6 chiffes, sans espaces ni caractères spéciaux");
			idClient = in.next();
			in.nextLine();
			} while (!Control.controlIdClient(idClient));
			
			System.out.println("Saisissez le nom du client");
			String nomClient = in.nextLine();
			
			System.out.println("Saisissez le prénom du client");
			String prenomClient = in.nextLine();
			
			do {
				System.out.println("Saisissez la date de naissance du client au format US (yyyy-mm-dd)");
				dateNaissance = in.nextLine();
			} while (!Control.controlFormatDate(dateNaissance));
			
			
			System.out.println("Saisissez l'adresse e-mail du client");
			String email = in.nextLine();
			
			
			do {
				System.out.println("Le client est-il actif ? Tapez actif ou inactif");
				etatClient = in.nextLine();
				if ("actif".equalsIgnoreCase(etatClient)) {
					actifInactif=true;
				} else if ("inactif".equalsIgnoreCase(etatClient)) {
					actifInactif=false;
				}
			} while (!"actif".equalsIgnoreCase(etatClient) && !"inactif".equalsIgnoreCase(etatClient));
			
			
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/cdabanque", "banquier", "123");
			String strQuery = "insert into client (id_client, nom_client, prenom, date_naissance, e_mail, client_actif) values (?, ?, ?, ?, ?, ?)";
			stServices = conn.prepareStatement(strQuery);
			stServices.setString(1, idClient);
			stServices.setString(2, nomClient);
			stServices.setString(3, prenomClient);
			stServices.setString(4, dateNaissance);
			stServices.setString(5, email);
			stServices.setBoolean(6, actifInactif);
			
			int row = stServices.executeUpdate();

			System.out.println("Client créé");

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			
		} catch (SQLException e) {
			System.out.println("Cet ID client existe déjà et doit être différent pour chaque client créé");
		} finally {
			
			// Fermeture du statement
			
			if (stServices != null)
				try {
					stServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			// Fermeture de la connection
			if (conn != null)
				try {

					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			

		}
	}
	
	
	/**
	 * Méthode à vérifications multiples car la table en question contient 2 clés étrangères qui peuvent être sources de conflit dans la base de données
	 * Contrôle si le client existe bien
	 * Contrôle si l'agence existe bien
	 */
	public static void creerCompte() {
		Scanner in = new Scanner(System.in);
		Connection conn = null;
		PreparedStatement stServices = null;
		ResultSet listeServices = null;
		boolean decouvertAutorise = false;
		String answerDecouvert=null;
		String codeAgence = null;
		String idClient=null;
		String query=null;

		
		try {
			
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);
			
			
			System.out.println("Saisissez l'id d'un client existant pour lequel vous souhaitez créer un compte");
			idClient = in.nextLine();	
			if (!Control.maxAccountsControl(idClient) || !Control.existClientControl(idClient)) {
				return;
			}
			
			System.out.println("Saisissez l'agence qui sera rattachée au compte bancaire en tapant le code correspondant à code_agence. Voici la liste des agences : "+"\n");
			listeAgences();
			codeAgence = in.nextLine();
			if (!Control.existAgenceControl(codeAgence)) {
				return;
			}
			
			System.out.println("Saisissez le solde du client");
			float solde = in.nextFloat();
			in.nextLine();
			
			
			do {
				System.out.println("Autorisation de découvert - Tapez oui ou non");
				answerDecouvert=in.nextLine();
				if (answerDecouvert.equalsIgnoreCase("oui")) {
					decouvertAutorise = true;
				} else if(answerDecouvert.equalsIgnoreCase("non")) {
					decouvertAutorise = false;
				}
			} while (!"oui".equalsIgnoreCase(answerDecouvert) && !"non".equalsIgnoreCase(answerDecouvert));
			
			
			
			
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/cdabanque", "banquier", "123");
			String strQuery = "insert into compte (num_compte, solde, decouvert, id_client, code_agence) values (nextval('sq_compte'), ?, ?, ?, ?)";
			stServices = conn.prepareStatement(query);
			stServices.setFloat(1, solde);
			stServices.setBoolean(2, decouvertAutorise);
			stServices.setString(3, idClient);
			stServices.setString(4, codeAgence);
			
			int row = stServices.executeUpdate();
			
			System.out.println("Compte bancaire créé");

			} catch (ClassNotFoundException e) {
			e.printStackTrace();
			
		} catch (SQLException e) {
			System.out.println("Cet ID client/code agence n'existe pas");
		} finally {
			
			// Fermeture du statement
			
			if (stServices != null)
				try {
					stServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			// Fermeture de la connection
			if (conn != null)
				try {

					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			

		}
	}
		
	
	/**
	 * Cette méthode a vocation à lister les différentes agences à l'utilisateur de sorte à choisir le code agence pour lequel il souhaite rattacher le compte d'un client
	 * Parce que le code est généré automatiquement par la séquence, l'utilisateur n'y a pas accès et il faut l'en informer
	 */
	public static void listeAgences() {
		Connection conn = null;
		PreparedStatement stServices = null;
		ResultSet listeServices = null;
		
		try {
			
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);
			
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/cdabanque", "banquier", "123");
			String strQuery = "select * from agence";
			stServices = conn.prepareStatement(strQuery);
			
			stServices.executeQuery();

			while (listeServices.next()) {

				System.out.println("code_agence :     " + listeServices.getString("code_agence"));
				System.out.println("nom_agence  :     " + listeServices.getString("nom_agence"));
				System.out.println("adresse_agence :  " + listeServices.getString("adresse_agence"));
				System.out.println("---------------------------------------------------------");
			}

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			
			// Fermeture du RS
			if (listeServices != null)
				try {
					listeServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			// Fermeture du statement
			
			if (stServices != null)
				try {
					stServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			// Fermeture de la connection
			if (conn != null)
				try {

					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			

		}
	}
	
	/**
	 * Méthode de recherche de compte par numéro de compte, retour de méthode booléenne pour vérifier si le compte existe.
	 * Utilisation de case when end dans la requête SQL pour transformer le t/f en autorisé/refusé.
	 */
	public static void rechercherCompte() {
		Scanner in = new Scanner (System.in);
		Connection conn = null;
		PreparedStatement stServices = null;
		ResultSet listeServices = null;
		String numCompte=null;
		
		try {
			
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);
			
			System.out.println("Tapez le numéro de compte à rechercher pour accéder aux informations du compte bancaire");
			numCompte = in.nextLine();
			if (!Control.existAccountControl(numCompte)) {
				return;
			}
			
			
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/cdabanque", "banquier", "123");
			String strQuery = "select num_compte, round(cast(solde as numeric), 2) as solde, (case when decouvert=true then 'autorisé' else 'refusé' end) as decouvert, id_client, code_agence from compte where num_compte = '"+numCompte+"'";
			
			stServices = conn.prepareStatement(strQuery);
			
			stServices.executeQuery();

			while (listeServices.next()) {

				System.out.println("Numéro de compte :   " + listeServices.getString("num_compte"));
				System.out.println("Solde  :             " + listeServices.getString("solde"));
				System.out.println("Découvert :          " + listeServices.getString("decouvert"));
				System.out.println("ID client :          " + listeServices.getString("id_client"));
				System.out.println("Code agence :        " + listeServices.getString("code_agence"));
				System.out.println("---------------------------------------------------------");
			}

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			
			// Fermeture du RS
			if (listeServices != null)
				try {
					listeServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			// Fermeture du statement
			
			if (stServices != null)
				try {
					stServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			// Fermeture de la connection
			if (conn != null)
				try {

					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			

		}
	}
	
	/**
	 *  Recherche de client à choix multiples par le biais d'un switch. Retour booleen d'un controle pour s'assurer que le client est actif.
	 *  Utilisation de la fonction case when end pour transformer le t/f en actif/inactif
	 */
	public static void rechercherClient() {
		Scanner in = new Scanner (System.in);
		Connection conn = null;
		PreparedStatement stServices = null;
		ResultSet listeServices = null;
		String numCompte= null;
		String nom=null;
		String idClient=null;
		String choix = null;
		
		try {
			
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);
			
			System.out.println("Tapez : A pour rechercher un client par nom"+"\n"+
					"B pour rechercher par numéro de compte"+"\n"+
					"C pour rechercher par id client");
			choix = in.nextLine();
			
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/cdabanque", "banquier", "123");
			switch (choix) {
			case "A" : { System.out.println("Tapez le nom du client à rechercher");
				nom=in.nextLine();
				
				
				String strQuery = "select id_client, nom_client, prenom, date_naissance, e_mail, (case when client_actif=true then 'actif' else 'inactif' end) as client_actif from client where nom_client = ? and client_actif = true";
				stServices = conn.prepareStatement(strQuery);
				stServices.setString(1, nom);
				stServices.executeQuery();

				while (listeServices.next()) {

					System.out.println("ID client :    " + listeServices.getString("id_client"));
					System.out.println("Nom  :         " + listeServices.getString("nom_client"));
					System.out.println("Prénom :       " + listeServices.getString("prenom"));
					System.out.println("Naissance :    " + listeServices.getString("date_naissance"));
					System.out.println("E-mail :       " + listeServices.getString("e_mail"));
					System.out.println("État client :  " + listeServices.getString("client_actif"));
					System.out.println("---------------------------------------------------------");
				}
				
				break; }
			
			case "B" : { System.out.println("Tapez le numéro de compte");
				numCompte=in.nextLine();	
				String strQuery = "select c.id_client, c.nom_client, c.prenom, c.date_naissance, c.e_mail, (case when c.client_actif=true then 'actif' else 'inactif' end) as client_actif from client c, compte cpt where c.id_client = cpt.id_client and cpt.num_compte = ? and c.client_actif = true";
				stServices = conn.prepareStatement(strQuery);
				stServices.setString(1, numCompte);
				stServices.executeQuery();

				while (listeServices.next()) {

					System.out.println("ID client :    " + listeServices.getString("id_client"));
					System.out.println("Nom  :         " + listeServices.getString("nom_client"));
					System.out.println("Prénom :       " + listeServices.getString("prenom"));
					System.out.println("Naissance :    " + listeServices.getString("date_naissance"));
					System.out.println("E-mail :       " + listeServices.getString("e_mail"));
					System.out.println("État client :  " + listeServices.getString("client_actif"));
					System.out.println("---------------------------------------------------------");
				}
				
				break; }
			
			case "C" : { System.out.println("Tapez l'idClient");
				idClient=in.nextLine();
				if (!Control.isClientActive(idClient)) {
					return;
				} else {
					
					String strQuery = "select id_client, nom_client, prenom, date_naissance, e_mail, (case when client_actif=true then 'actif' else 'inactif' end) as client_actif from client where id_client = ?";
					stServices = conn.prepareStatement(strQuery);
					stServices.setString(1, idClient);
					stServices.executeQuery();

					while (listeServices.next()) {

						System.out.println("ID client :    " + listeServices.getString("id_client"));
						System.out.println("Nom  :         " + listeServices.getString("nom_client"));
						System.out.println("Prénom :       " + listeServices.getString("prenom"));
						System.out.println("Naissance :    " + listeServices.getString("date_naissance"));
						System.out.println("E-mail :       " + listeServices.getString("e_mail"));
						System.out.println("État client :  " + listeServices.getString("client_actif"));
						System.out.println("---------------------------------------------------------");
					}
				}
				
				break; }	
			}
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			
			// Fermeture du RS
			if (listeServices != null)
				try {
					listeServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			// Fermeture du statement
			
			if (stServices != null)
				try {
					stServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			// Fermeture de la connection
			if (conn != null)
				try {

					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			

		}
	}
	
	/**
	 * La requête SQL vise à faire une jointure entre les tables client et compte de sorte à récupérer la liste des comptes en fonction de l'id client saisi par l'utilisateur
	 */
	public static void afficherComptesClient() {
		
		Scanner in = new Scanner (System.in);
		Connection conn = null;
		PreparedStatement stServices = null;
		ResultSet listeServices = null;
		String idClient=null;
		
		try {
			
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);
			
			System.out.println("Tapez l'idClient");
			idClient=in.nextLine();
			if (!Control.isClientActive(idClient)) {
				return;
			}
			
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/cdabanque", "banquier", "123");
			String strQuery = "select cpt.num_compte, round(cast(cpt.solde as numeric), 2) as solde, (case when cpt.decouvert = true then 'autorisé' else 'refusé' end) as decouvert, cpt.id_client, cpt.code_agence from compte cpt, client c where cpt.id_client = c.id_client and c.id_client = ?";
			stServices = conn.prepareStatement(strQuery);
			stServices.setString(1, idClient);
			stServices.executeQuery();

			while (listeServices.next()) {
				
				System.out.println("Numéro de compte :   " + listeServices.getString("num_compte"));
				System.out.println("Solde  :             " + listeServices.getString("solde"));
				System.out.println("Découvert :          " + listeServices.getString("decouvert"));
				System.out.println("ID client :          " + listeServices.getString("id_client"));
				System.out.println("Code agence :        " + listeServices.getString("code_agence"));
				System.out.println("---------------------------------------------------------");
			}

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			
			// Fermeture du RS
			if (listeServices != null)
				try {
					listeServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			// Fermeture du statement
			
			if (stServices != null)
				try {
					stServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			// Fermeture de la connection
			if (conn != null)
				try {

					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			

		}
		
	}
	
	/**
	 * Cette méthode permet à l'utilisateur de désactiver un client. La méthode de contrôle vérifie si le client est déjà désactivé.
	 */
	public static void desactiverClient() {
		Scanner in = new Scanner (System.in);
		Connection conn = null;
		PreparedStatement stServices = null;
		ResultSet listeServices = null;
		String idClient=null;
		
		try {
			
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);
			
			System.out.println("Tapez l'idClient");
			idClient=in.nextLine();
			if (!Control.isClientActive(idClient)) {
				return;
			}
			
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/cdabanque", "banquier", "123");
			String strQuery = "update client set client_actif = false where id_client = ?";
			stServices = conn.prepareStatement(strQuery);
			stServices.setString(1, idClient);
			stServices.executeQuery();
			
			System.out.println("Client désactivé");

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			
			// Fermeture du statement
			
			if (stServices != null)
				try {
					stServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			// Fermeture de la connection
			if (conn != null)
				try {

					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			

		}
	}
	
	/**
	 * Suppression de compte. Il convient d'indiquer à l'utilisateur de saisir le numéro de compte à supprimer en lui affichant la liste des comptes avec toutes les colonnes
	 * de sorte à ce qu'il puisse voir à quel client et à quelle agence le compte est rattaché.
	 */
	public static void supprimerCompte() {
		Scanner in = new Scanner (System.in);
		Connection conn = null;
		PreparedStatement stServices = null;
		ResultSet listeServices = null;
		String bankAccount=null;
		
		try {
			
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);
			
			System.out.println("Choisissez le compte bancaire à supprimer en saisissant le numéro de compte de la colonne num_compte parmi la liste des comptes suivante : "+"\n");
			listeComptes();
			bankAccount=in.nextLine();
			
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/cdabanque", "banquier", "123");
			String strQuery = "delete from compte where num_compte = ?";
			stServices = conn.prepareStatement(strQuery);
			stServices.setString(1, bankAccount);
			stServices.executeQuery();
			

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			
		} catch (SQLException e) {
			System.out.println("Impossible de supprimer un compte qui n'existe pas");
		} finally {
			
			// Fermeture du statement
			
			if (stServices != null)
				try {
					stServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			// Fermeture de la connection
			if (conn != null)
				try {

					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

		}
	}
	
	/**
	 * Liste tous les comptes bancaires à destination de la méthode  supprimerCompte()
	 */
	public static void listeComptes() {
		Connection conn = null;
		PreparedStatement stServices = null;
		ResultSet listeServices = null;
		
		try {
			
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);
			
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/cdabanque", "banquier", "123");
			String strQuery = "select num_compte, round(cast(solde as numeric), 2) as solde, (case when decouvert=true then 'autorisé' else 'refusé' end) as decouvert, id_client, code_agence from compte";
			stServices = conn.prepareStatement(strQuery);
			stServices.executeQuery();

			while (listeServices.next()) {

				System.out.println("Numéro de compte :   " + listeServices.getString("num_compte"));
				System.out.println("Solde  :             " + listeServices.getString("solde"));
				System.out.println("Découvert :          " + listeServices.getString("decouvert"));
				System.out.println("ID client :          " + listeServices.getString("id_client"));
				System.out.println("Code agence :        " + listeServices.getString("code_agence"));
				System.out.println("---------------------------------------------------------");
			}

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			
			// Fermeture du RS
			if (listeServices != null)
				try {
					listeServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			// Fermeture du statement
			
			if (stServices != null)
				try {
					stServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			// Fermeture de la connection
			if (conn != null)
				try {

					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			

		}
	}
	
}