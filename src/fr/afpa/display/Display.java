package fr.afpa.display;

import java.util.Scanner;

import fr.afpa.model.GestionBanque;
import fr.afpa.model.PrintPDF;

public class Display {

	static Scanner in = new Scanner(System.in);
	static String choice = null;
	
	
	/**
	 * Interface utilisateur articulé par un menu. Appel des différentes méthodes de la classe model GestionBanque
	 */
	public static void menu() {
		do {
			choix();
			choice = in.next();
			switch(choice) {
			case "A" : { GestionBanque.creerAgence(); break;}
			case "B" : { GestionBanque.creerClient(); break;}
			case "C" : { GestionBanque.creerCompte(); break; }
			case "D" : { GestionBanque.rechercherCompte(); break; }
			case "E" : { GestionBanque.rechercherClient(); break; }
			case "F" : { GestionBanque.afficherComptesClient(); break;}
			case "G" : { GestionBanque.desactiverClient(); break; }
			case "H" : { GestionBanque.supprimerCompte(); break; }
			case "I" : { PrintPDF.retrieveAccounts(); break; }
			}
		} while (choice != "Z");
	}
	
	
	/**
	 * Les choix proposés par le menu
	 */
	public static void choix() {
		System.out.println( "Tapez A pour créer une agence"+"\n"+
							"Tapez B pour créer un client"+"\n"+
							"Tapez C pour créer un compte bancaire"+"\n"+
							"Tapez D pour rechercher un compte"+"\n"+
							"Tapez E pour rechercher un client"+"\n"+
							"Tapez F pour afficher les comptes d'un client"+"\n"+
							"Tapez G pour désactiver un client"+"\n"+
							"Tapez H pour supprimer un compte"+"\n"+
							"Tapez I pour imprimer les infos d'un client"+"\n"+
							"Tapez Z pour quitter le programme");
	}
	
}
