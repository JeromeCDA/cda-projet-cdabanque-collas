package fr.afpa.beans;

import java.sql.Date;

public class Client {

	private String id;
	private String nomClient;
	private String prenom;
	private Date date;
	private String email;
	private boolean clientActif;
	private Compte compte [];
	
	
	public Client(String id, String nomClient, String prenom, Date date, String email, boolean clientActif,
			Compte[] compte) {
		super();
		this.id = id;
		this.nomClient = nomClient;
		this.prenom = prenom;
		this.date = date;
		this.email = email;
		this.clientActif = clientActif;
		this.compte = new Compte [3];
	}
	
	public Client () {	
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNomClient() {
		return nomClient;
	}

	public void setNomClient(String nomClient) {
		this.nomClient = nomClient;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isClientActif() {
		return clientActif;
	}

	public void setClientActif(boolean clientActif) {
		this.clientActif = clientActif;
	}

	public Compte[] getCompte() {
		return compte;
	}

	public void setCompte(Compte[] compte) {
		this.compte = compte;
	}
	
	public String toString() {
		return "id : "+this.id+"\n"+
		"Nom client : "+this.nomClient+"\n"+
		"Prénom client : "+this.prenom+"\n"+
		"Date de naissance : "+this.date+"\n"+
		"Email : "+this.email+"\n"+
		"Statut client (actif/inactif) : "+this.clientActif+"\n"+
		"Comptes client : "+this.compte;
	}
	
	
}
