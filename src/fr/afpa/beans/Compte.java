package fr.afpa.beans;

public class Compte {

	private float solde;
	private boolean decouvert;
	
	
	public Compte(float solde, boolean decouvert) {
		super();
		this.solde = solde;
		this.decouvert = decouvert;
	}
	
	public Compte() {
	}

	public float getSolde() {
		return solde;
	}

	public void setSolde(float solde) {
		this.solde = solde;
	}

	public boolean isDecouvert() {
		return decouvert;
	}

	public void setDecouvert(boolean decouvert) {
		this.decouvert = decouvert;
	}
	
	
	public String toString() {
		return solde+""+decouvert;
	}
	
}
