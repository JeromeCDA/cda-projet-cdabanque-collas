package fr.afpa.beans;

public class Agence {
	
	private String nom;
	private String adresse;
	private Client client;
	

	public Agence(String nom, String adresse, Client client) {
		super();
		this.nom = nom;
		this.adresse = adresse;
		this.client = client;
	}


	public Agence() {
		
	}
	
	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public String getAdresse() {
		return adresse;
	}


	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}


	public Client getClient() {
		return client;
	}


	public void setClient(Client client) {
		this.client = client;
	}


	public String toString() {
		return this.nom+this.adresse;
		
	}
	
	

}
